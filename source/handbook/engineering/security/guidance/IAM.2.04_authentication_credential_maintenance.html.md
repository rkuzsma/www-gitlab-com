---
layout: markdown_page
title: "IAM.2.04 - Authentication Credential Maintenance"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# IAM.2.04 - Authentication Credential Maintenance
 
## Control Statement
Authorized personnel verify the identity of users before modifying authentication credentials on their behalf.
 
## Context
Verifying the identity of users before making authentication changes prevents attackers from impersonating someone in order to gain access to their credentials.
 
## Scope
This control applies to:
   * GitLab.com
   * System that support the operation of GitLab.com
   * All applications that store or process financial data

## Ownership
TBD
 
## Guidance
TBD
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Authentication Credential Maintenance issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/815) . 
 
### Policy Reference
TBD
 
## Framework Mapping
TBD
