---
layout: handbook-page-toc
title: "Strategic Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Strategic marketing at GitLab

### Strategic marketing department at GitLab includes the following teams

<table width="100%">
  <tr>
    <td><a href="/handbook/marketing/product-marketing/pmmteam/"><img src="/handbook/marketing/product-marketing/images/product-marketing.png" alt="Product Marketing"></a></td>
    <td><a href="/handbook/marketing/product-marketing/technical-marketing/"><img src="/handbook/marketing/product-marketing/images/technical-marketing.png" alt="Technical Marketing"></a></td>
    <td><a href="/handbook/marketing/product-marketing/partner-marketing/"><img src="/handbook/marketing/product-marketing/images/partners-channel-marketing.png" alt="Partner Marketing"></a></td>
    <td><a href="/handbook/marketing/product-marketing/competitive/"><img src="/handbook/marketing/product-marketing/images/competitive-intelligence.png" alt="Competitive Intelligence"></a></td>
    <td><a href="/handbook/marketing/product-marketing/mrnci/"><img src="/handbook/marketing/product-marketing/images/market-customer-research.png" alt="Market Research and Customer Refereneces"></a></td>
  </tr>
  <tr>
    <td colspan="5"><a href="/handbook/sales/training/"><img src="/handbook/marketing/product-marketing/images/enablement.png" atl="Enablement: Sales, Partners, Resellers"></a></td>
  </tr>
</table>

1. [Product marketing team](/handbook/marketing/product-marketing/pmmteam/)
1. [Technical marketing team](/handbook/marketing/product-marketing/technical-marketing/)
1. [Partner marketing team](/handbook/marketing/product-marketing/partner-marketing/)
1. [Channel marketing team](/handbook/marketing/product-marketing/analyst-relations/channel-marketing/)
1. [Competitive intelligence team](/handbook/marketing/product-marketing/competitive/)
1. [Market research and customer insight team](/handbook/marketing/product-marketing/mrnci/)
  - [Analyst relations (AR)](/handbook/marketing/product-marketing/analyst-relations/)
  - [Customer reference program](/handbook/marketing/product-marketing/customer-reference-program/)

### What does Strategic Marketing do at GitLab

  Strategic marketing is GitLab's interface to the market. The market is made up of customers, analysts, press, thought leaders, competitors, etc. Strategic marketing enables other GitLab teams such as Sales, Marketing, and Channel with narrative, positioning, messaging, and go-to-market strategy to go outbound to the market. Strategic marketing does market research to gather customer knowledge, analyst views, market landscapes, and competitor intelligence providing marketing insights inbound to the rest of GitLab.

  ![Strategic marketing functions](/handbook/marketing/product-marketing/images/ProductMarketingFunctions.png)

### Key content created by Strategic Marketing

  Strategic marketing creates many types of content for communicating and positioning GitLab for multiple audiences.  Some of the different kinds of content produced by Strategic Marketing are listed below.

  ![Strategic marketing functions](/handbook/marketing/product-marketing/images/PMMOutput.png)

### Some key resources

#### Customer facing presentations

  Marketing decks linked on this page are the latest approved decks from Strategic Marketing that should be always be in a state that is ready to present. As such there should never be comments or WIP slides in a marketing deck. If you copy the deck to customize it please give it a relevant title, for example include the name of the customer and an ISO date.

  - [Company pitch deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)  
    The Pitch Deck contains the GitLab narrative.

  - [Customer value deck](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing)  
      The Customer deck contains the GitLab value driver narrative and supports a discussion about GitLab differentiators.

  - [SDR presentation deck](https://docs.google.com/presentation/d/1679lQ7AG6zBjQ1dnb6Fwj5EqScI3rvaheIUZu1iV7OY/edit#slide=id.g42cc3032dd_1_1903)    
    Condensed version of the company pitch deck. It copies linked slides from the pitch deck (so they can stay in sync.) SDR Managers own responsibility for keeping the deck in sync.  

    This deck can be used on occasions where the SDRs feel they should or could prequalify a prospect before setting a discovery meeting with the SAL. This could for example be someone who isn't our typical target persona but who might have an interest in what we do.  

  - [GitLab security capabilities deck](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing)  
    This deck introduces GitLab's position and capabilities around security. It covers why better security is needed now and how GitLab provides that better security in a more effective manner than traditional tools. This deck should be used when talking to prospects who are asking about how GitLab can help them better secure their software via GitLab Ultimate.

  To request updates to these decks see [requesting help from the strategic marketing department](#requesting-strategic-marketing-team-helpsupport)


#### Key Demos
##### Videos
   - [GitLab in 3 minutes](https://youtu.be/Jve98tlZ394)
   - [Benefits of a Single Application](https://youtu.be/MNxkyLrA5Aw)
   - [GitLab Overview - 18 minutes](https://youtu.be/nMAgP4WIcno)

##### Click through Demos
   - [Auto DevOps](https://docs.google.com/presentation/d/1oKHU3MsbJmxVQyO-7c6JLMoCOS80uS-0NlcI-mRxAAY/edit?usp=sharing)
   -  [GitLab Secure Capabilities](https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9)
   - [GitLab Agile Project Management](https://docs.google.com/presentation/d/13Zj83pjpwyq3s4T2fPSTuKO8NwqdCdn827GB7S-3hW8/edit?usp=sharing)

#### Print collateral   
  The following list of print collateral is maintained for use at trade shows, prospect and customer visits.  The GitLab marketing pages should be downloaded as PDF documents if a print copy for any marketing purposes is needed.
  - [GitLab DataSheet](/images/press/gitlab-data-sheet.pdf)
  - [GitLab Federal Capabilities One-pager](/images/press/gitlab-capabilities-statement.pdf)
  - [How GitLab is Enterprise Class](/solutions/enterprise-class)
  - [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
  - [Speed to mission whitepaper](/resources/downloads/201906-whitepaper-speed-to-mission.pdf)

#### Usecase based messaging
  The Strategic Marketing team also develops messaging and collaterals that aligns with a buyer's needs and a journey with GitLab supporting those needs. For example, a customer with a specific problem of SCM does not need to be sold the value of the single application. Rather these usecase based messaging and collaterals will help when talking to customers to address their specific pain points.
  - [Why Usecase driven GTM?](/handbook/marketing/product-marketing/usecase-gtm/)
  - [Usecase: Simplify DevOps](/handbook/marketing/product-marketing/usecase-gtm/simplify-devops)
  - Usecase: Source Code Management
  - Usecase: Continuous Integration
  - Usecase: Continuous Deployment
  - Usecase: DevSecOps
  - Usecase: Agile Project Management
  - Usecase: Cloud Native Development
  - Usecase: GitOps


#### Speaker Abstracts    
  To encourage reuse and collaboration, we have a [shared folder of past abstracts](https://drive.google.com/drive/folders/1ODXxqd4xpy8WodtKcYEhiuvzuQSOR_Gg) for different speaking events.  

#### Strategic marketing - team specific planning and reporting resources  
  - [Strategic marketing group conversation slides](https://drive.google.com/drive/folders/1fCEAj1HCegJOJE_haBqxcy2NYm0DS1FO)   
  - [Strategic marketing FY 2020 Vision](https://docs.google.com/presentation/d/1sbpBNy5OpO0QGvkAeobNyyIcEjTRGIkyApKeC1Oa8xY/edit#slide=id.g4a712342f9_0_852)
  - [CMO Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/1160244?label_name[]=CMO%20Staff%20Triage)
  - [Strategic Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name[]=sm_request)
  - [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name[]=tech-pmm)
  - [Partner Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/-/boards/814970)
  - [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program)
  - [Case Studies Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/918204?&label_name[]=Case%20study)
  - [Strategic Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name[]=sm_request)
  - [Sales Enablement Board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement)
  - [SDR Coaching Board](https://gitlab.com/gitlab-com/marketing/general/boards/772948?label_name[]=XDR-Coaching)
  - [PMM Budget Planning](https://docs.google.com/spreadsheets/d/1_MN8K9ixdgOp32DKiGPjT-208pr8s-rNKKuuYHsqCdI/edit#gid=1423447843)
  - [PMM Hiring Planning](https://docs.google.com/spreadsheets/d/12mNijMwA8hIG5h3zV5JJ0oxsVh6xzk6-si0eT7L9mvM/edit#gid=1301737184)
  - [Strategic Marketing Event and Travel Priorities](/handbook/marketing/product-marketing/events_travel/)


#### Other useful resources to help your productivity  
  - [Getting Started - GitLab 101 - No Tissues for Issues](/handbook/marketing/product-marketing/getting-started/101/)  
  - [Getting Started - GitLab 102 - Working Remotely](/handbook/marketing/product-marketing/getting-started/102/)
  - [Getting Started - GitLab 103 - Maintaining common slides across decks](/handbook/marketing/product-marketing/getting-started/103/)
  - [Markdown style guide for about.gitlab.com](/handbook/engineering/technical-writing/markdown-guide/#markdown-style-guide-for-aboutgitlabcom)
  - [Searching the GitLab Website Like a Pro](/handbook/tools-and-tips/searching/)
  - [Other frequently used sales resources](/handbook/marketing/product-marketing/sales-resources/)  

### Requesting Strategic marketing team help/support
All Strategic Marketing Work is tracked and managed as issues in the Strategic Marketing Project. If you need support from the team, the simple process below will enable us to support you.
1. [Open an SM Support Request Issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new?issuable_template=A-SM-Support-Request) this link will the *A-SM-Support-Request* template. PLEASE fill in what you know.
1. The Strategic Marketing leadership team will review the request, assign it to a SM Team, prioritize the work and plan how to support your requests.
1. If you need more immediate attention please send a message with a link to the issue you created in the `#product-marketing` slack channel. Add an `@reply` to the PMM responsible or you can ping the team with `@pmm-team`.

![SM Request Flow](/handbook/marketing/product-marketing/images/SM_Request_FLow_V3.png)

**Strategic marketing request review and assignment flow** (note: the label `sm_request` indicates a request for Strategic Marketing support)  
1. **New requests** start with the label `sm_req::new_request`.
   1. **Triaged** to one of the Strategic Marketing teams (PMM,TMM, Competitive Intel, Partner Marketing, Market Research/Cust Insight) `sm_req::triage` and the team label (`pmm`,`tech-pmm`,`Competitive Intelligence`, `Partner Marketing`, `mrci`) who will determine if if there is enough detail to prioritize and plan the work - is it clear?   Then the issue will be route to either:
   1. **Backlog** `sm_req::backlog` for future scheduling, sequencing, and implementation.  *note: add issue to the SM_Backlog milestone for tracking.* **NOTE: Issues in the backlog are NOT yet committed to be done!**
   1. **Assigned** `sm_req::assigned` to team members.   When an issue is assigned, it is added to the quarter **milestone** so we can track status of all the work in flight. **NOTE: Assigned issues should be considered committed to be done!**
   1. *Transferred* `sm_req::transferred` for requests that belong in a different team (Field Marketing, Sales, Ops, etc). Once an issue is transferred, it should be **closed**
   1. *declined* `sm_req::declined` - when an issue is in the backlog and it is no longer relevant or does not make sense anymore.  **Close** the issue when you *decline* it.
1. When **complete**, the team member will update the issue with `sm_req::completed` and **Close** the issue

#### Managing the request process

GitLab provides several ways to visualize and manage our work:
1. [SM Request Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?&label_name[]=sm_request) provides a way to visualize the tracks status of requests grouped by labels.  There are several limitations: - lots of scrolling, no sorting, only one action when dragging.   So - these different views may be more useful:
1. **List Views**: enable you to view multiple issues in a common status and you can sort, filter, and make mutliple updates to multiple issues in one step.  See the **Edit Issue** button at the top right.
   1. [Strategic NEW REQUEST List View](https://gitlab.com/gitlab-com/marketing/product-marketing/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=sm_req%3A%3Anew_request) This view provides a way to see ALL the New Requests in one list for all of Strategic Marketing.  
   1. [Strategic Marketing TRIAGE view](https://gitlab.com/gitlab-com/marketing/product-marketing/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=sm_req%3A%3Atriage) This view is all of the Triaged issues.
   1. [Strategic Marketing ASSIGNED view](https://gitlab.com/gitlab-com/marketing/product-marketing/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=sm_req%3A%3Aassigned) This view of all the issues assigned.
   1. [Strategic Marketing Backlog view](https://gitlab.com/gitlab-com/marketing/product-marketing/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=sm_req%3A%3Abacklog)
   1. [Assigned and PMM view](https://gitlab.com/gitlab-com/marketing/product-marketing/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=sm_req%3A%3Aassigned&label_name[]=pmm) View shows all the PMM team issues that are currently assigned.
1. **Milestones**: Milestones give us the ability to track issue completion over time.   Milestones also make it easy to look at the related issues based on specific labels and on who the issues are assigned to. Currently, GitLab ONLY allows for one milestone per issue, a limitation that will change in the 12.7 release of GitLab release. In Strategic Marketing, we're using two different Milestones to visualize status:
   1. [Current Quarter Work in progress](https://gitlab.com/gitlab-com/marketing/product-marketing/-/milestones/4) - this is a **PRIMARY VIEW** to see what we're working on and the progress toward closure for the overall team.  At the end of the quarter, we will create a new milestone for the next quarter **and** move any unfinished issues to the new milestone.  (and potentially label them as being from a previous milestone.)
   1. [Strategic Marketing Backlog](https://gitlab.com/gitlab-com/marketing/product-marketing/-/milestones/5) - this is a view of our current backlog which makes it easy to navigate to groups of related issues based on label such as (Priority, team, or usecase)
1. [**Quick Actions**](https://gitlab.com/help/user/project/quick_actions): - Quick actions are commands that you can enter into a comment on an issue and quickly update the status of the issue.  Through quick actions you can
- add or remove a label
- assign or unassign the issue to someone
- add or remove from a milestone
- open or close the issues

Quick actions are **very,very** helpful and efficient when you want to make multiple changes to an issue and have the issue open. Here are a few handy quick actions for Strategic Marketing:

|  **Step / Action** |  **Quick Action Code** |
|-----|------|
| **Triage** for the **PMM team** |  `/Label ~"sm_req::triage" ~pmm`  |
| **Triage** for the **Tech PMM team**  | `/Label ~"sm_req::triage" ~tech_pmm`  |
| **Triage** for the **Partner Marketing team** |  `/Label ~"sm_req::triage" ~"Partner Marketing"` |
| **Triage** for the **Competitive Intel team** |  `/Label ~"sm_req::triage"  ~"Competitive Intelligence"` |
| **Triage** for **Market Research/Customer Ref Team** |  `/Label ~"sm_req::triage" ~mrci` |
| **Moving to backlog** |  `/Label ~"sm_req::backlog"` <br> `/Milestone %"SM - Backlog"` |
| **Assigning to a team member** |  `/Label ~"sm_req::assigned" ~"status::wip"` <br> `/Milestone %Q4FY20` <br> `/Assign @<TeamMember>`|
| **Completing an issue** |  `/Label ~"sm_req::completed"` <br> `/close` |


#### Backlog Grooming and prioritization

In order to manage and prioritize issues in the backlog, we need to consider:

  - Our capacity and current work in progress
  - DevOps Stage: (`/label ~"devops::manage"`, `/label ~"devops::plan" ~"devops::create" `, `/label~"devops::verify" `, `/label~"devops::package" `, `/label~"devops::secure" `, `/label~"devops::release" `, `/label~"devops::configure" `, `/label~"devops::monitor" `, `/label~"devops::defend"`)
  - DevOps Category
  - Customer Usecase:  (`/label ~"usecase::SCM"`, `/label ~"usecase::CI"`, `/label ~"usecase::CD"`, `/label ~"usecase::Agile"`, `/label ~"usecase::AppSec"`, `/label ~"usecase::DevOps"`, `/label ~"usecase::CN"`, `/label ~"usecase::GitOps"`
  - GTM phase:  (`/label ~"Stage::Awareness"`, `/label ~"Stage::Consideration"`, `/label ~"Stage::Purchase"`)

When considering an issue:   

  - Is the Definition of Done Clearly Defined? (dod::Y, or dod::N)
  - Is this an E team request? (eteam::Y, eteam::N)
  - Is there a critical due date?
  - What priority do we assign this item?  (p::1, p::2, p::3, p::?)
  `/Label ~"p::1"` or `/Label ~"p::2"` or `/Label ~"p::3"`

### Sales and partner enablement

Strategic Marketing team members serve as subject matter experts and conduct sales trainings scheduled by the [Sales Training team](/handbook/sales/training/). For for info go to the [Sales Enablement page](/handbook/sales/training/sales-enablement-sessions/).

### Strategic Marketing Hiring Process - What to Expect

1. Application Screening - Recruiter and Hiring Managers screen applications, resumes, and cover letters to select potential candidates.
2. Recruiter Screening - The recruiter schedules a brief call to discuss the role and the candidates potential fit.
3. Interviews - the typical sequence of interviews follows three stages.
    1. Hiring Manager Interview
    2. Team interviews - typically 3 - 4 separate interviews with different potential co-workers (product, sales, etc.)
    3. Executive interview with the Sr. Director of the Strategic Marketing team, the CMO, or even the CEO
