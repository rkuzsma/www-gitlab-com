---
layout: handbook-page-toc
title: Package Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Package Group

The Package group works on the part of GitLab concerning the [Package
stage], which integrates with [GitLab's CI/CD product](/direction/cicd/).
Our mission is to create a secure environment where both source code and dependencies can live by
allowing you to publish, consume, and discover packages of a large variety of languages and platforms
all in one place.

For more details about the vision for this area of the product, see the [product vision] page.

[product vision]: /direction/package/
[Package stage]: /handbook/product/categories/#package-stage

## Team Members

The following people are permanent members of the Package Group:

<%= direct_team(manager_role: 'Engineering Manager, Package') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Package/, direct_manager_role: 'Engineering Manager, Package') %>

## Issue boards

* [Package:Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1196366?&label_name[]=devops%3A%3Apackage&label_name[]=group%3A%3Apackage) - Issue board showing milestone planning for current and future milestones.
* [Package:Assignments](https://gitlab.com/groups/gitlab-org/-/boards/1200765?&label_name[]=group%3A%3Apackage) - Issue board organized by assignee to give a snapshot of who is working on what.
* [Package:Bugs](https://gitlab.com/groups/gitlab-org/-/boards/1200744?&label_name[]=bug&label_name[]=devops%3A%3Apackage) - Issue board displaying priority and severity of package related bugs.
* [Package: Validation Track](https://gitlab.com/groups/gitlab-org/-/boards/1397751?&label_name[]=group%3A%3Apackage)
* [Package:Scheduling](https://gitlab.com/groups/gitlab-org/-/boards/1382732?&label_name[]=devops%3A%3Apackage&label_name[]=group%3A%3Apackage)
* [Package:Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1284221?label_name[]=group%3A%3Apackage) - Issue board broken down by workflow label to give a snapshot of the status of in-progress issues.

## Demos & Speedruns
- [Conan Repository Demo](https://youtu.be/7NYgJWg-w5w)
- [Remote hierarchy for the Package Registry (project vs. group vs. instance level)](https://youtu.be/lVXfMKerZnQ)
- [How to host all your packages in a single project](https://youtu.be/ui2nNBwN35c)
- [How to remove Docker images using CI/CD (speedrun)](https://youtu.be/jDlFCrH9H7g)

## Think BIG planning
The purpose of this meeting is to discuss the vision, product roadmap, user research, design, and delivery around the Package solution.

### Goal
The goal of this meeting will be to align the team on our medium to long-term goals and ensure that our short-term goals are leading us in that direction. 

### How it works
We have a ThinkBIG meeting on the second Wednesday of the month at 1:30PM UTC. The agenda document is available per [communication guidelines](/handbook/communication/#scheduling-meetings) and the video of the meeting is shared on our [GitLabUnfiltered YouTube channel](https://www.youtube.com/c/GitLabUnfiltered) to enable asynchronous collaboration. Action items from the meeting will be created with the label `~Package:ThinkBIG`. We evaluate the cadence of the meeting to make sure that it's valuable and in adherance to [meeting guidelines](/company/culture/all-remote/meetings/).

### How to contribute
If possible, join the synchronous meeting and discussion on Wednesdays
Add discussion items to the [agenda document](https://docs.google.com/document/d/1G0Q4Cl19R0lx7VQbkZ4HmX7u6OpW4UJzO9pLemThjlE/edit#heading=h.p454vh4ftc5o)
Read through the active epics, leave feedback and questions
Read through the discussion topic issues and leave feedback/questions


## Architecture Forum

The architecture forum is intended to take ideas from the Think Big sessions and form them into more actionable issues. This meeting is also meant for the team to discuss technical issues to help find solutions, examples might be finding an implementation resulting in performance improvement or, how to store or capture data related to one of our Categories.

## How we work

### Roles and responsibilities

Our team emphasises ownership by people who have the information required. This means, for example, in the event of some discussion about UX considerations, our Product Designer will have ownership. When we're building features, the Product Manager owns the decision on whether this is a feature that meets our customer needs. Our Product Developers own the technical solutions being implemented.

### Understanding our users

As a team, we are committed to understanding our users needs. We believe the best way to do that is by understanding the reason they hired GitLab, and how those motivations translate into our area of the product. For that, we have apply a research-driven approach to Clayton M. Christensen's [Jobs to be Done (JTBD)](https://hbr.org/ideacast/2016/12/the-jobs-to-be-done-theory-of-innovation) theory of innovation, which aims to understand why a customer bought a given product. We then utilize the [job story](https://jtbd.info/replacing-the-user-story-with-the-job-story-af7cdee10c27) to identify a list of specific, contextual user needs to fulfill their JTBD.  In addition, we regularly evaluate the overall user experience of each JTBD, to ensure that we are meeting the needs of our users. You can view and contribute to our current list of JTBD and job stories [here](https://gitlab.com/package-stage/team/issues/25). All of the above is used to drive our validation and development workflows.

### Workflow

Our Product Manager owns the problem validation backlog and problem validation process as outlined in the [Product Development Workflow](https://about.gitlab.com/handbook/product-development-flow/). Our Product Designer then owns the [solution validation](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-3-solution-validation) process. You can view all items and their current state in the [Package: Validation Track](https://gitlab.com/groups/gitlab-org/-/boards/1397751?&label_name[]=group%3A%3Apackage) issue board.

At the end of this process, the issues will be ready for the [build track](/handbook/product-development-flow/#build-track) which is owned by the Product Developers and lives in the [Package:Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1284221?label_name[]=group%3A%3Apackage) issue board.

Once an issue receives the `workflow:scheduling` label, engineers will give the issue a weight, identify any gaps in the description moving the issue eventually to the `workflow:ready for development` state (with the matching label).

Our Product Manager, Engineering Manager and, Front End Engineering Manager will ensure that the `package:active` label is applied to sufficient issues for the team to have work items. Product Developers are empowered to move items into this state as well.

The Product Manager owns the process of populating the current milestone with feature work. This feature work will take priority but, will be limited to 3 to 4 items per milestone. Product Developers are empowered, once feature work has been exhausted, to prioritise customer value issues that will quickly deliver customer value preferring smaller issues over larger ones.

Issues that we're actually expecting to work on will have the `package:active` label added to them at which point they'll appear in the Package:Workflow board. As product developers begin working on the issue, they'll assign the `workflow:in dev` label.

### Priorities
Throughout the workflow, issues should be addressed in the following priority order:
1. Security issues
1. `Planning Priority` label: Used to identify the highest priority item in a given milestone
1. `Package::P1` label: Used to identify high priority issues that should be committed to in a given milestone
1. `package:triage` label: Used to identify issues, that although may not be in a milestone, should be worked on ahead of any other work
1. `package:active` least effort to largest

The `package:triage` label is applied to cross functional dependencies and issues that are blocking other work. Additionally, bugs that are coming up on their service level objectives.

### Code Review

Code reviews follow the standard process of using the [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) to choose a reviewer and a maintainer. The roulette is optional, so if a merge request contains changes that someone outside our group may not fully understand in depth, people are encouraged to ask for a preliminary review from members of the package group to focus on correctly solving the problem. The intent is to leave this choice to the discretion of the engineer but raise the idea that fellow package group members will sometimes be best able to understand the implications of the features we are implementing. The maintainer review will then be more focused on quality and code standards.

This tactic also creates an environment to ask for early review on a WIP merge request where the solution might be better refined through collaboration and also allows us to share knowledge across the team.

#### UI or Technical Writing Review

When a merge request needs to be reivewed for the experience or for the copy in the user interface, there are a few suggestions to ensure that the review process is quick and effecient: 
- When the MR has a UX or copy review, we suggest initating that part of the review process first to avoid experience changes in the middle of code reviews.
- A Product Designer will perform the UX review as well as the text review. If there is substantial change, the product designer may bring in a technical writer for a more thorough copy/content related review.
- If the tech writer us unavailable and the MR is being slowed down, it is possible to create a follow up issue for the copy or documentation to be reviewed post-merge. This should be avoided when possible.


### Community Contributions

#### Definition
A merge request with the following properties:
1. It impacts features or issues managed by the Package group. This means it has the ~"devops::package" label
2. Anyone in the wider community or at GitLab who isn't part of the Package group.

#### Handling
A Package group member will adopt the community contribution with the following tasks:
* Set aside some time per week to assist and coach the community contributor.
* Make sure that the merge request has the proper labels
* Make sure that the effort is reviewed by the product manager and the product designer
* Make sure that the reviews are happening. You can even suggest reviewers.

Other points to consider for the Package group member:
* The coaching can range from commenting/reviewing the merge request to pair programming through Zoom.
* Contributing to features managed by the Package group can mean having to use the Enterprise Edition (EE) version of GitLab. This [guideline](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/#for-contributors-to-the-gitlab-enterprise-edition-ee) will help with this aspect.
* Make sure that the merge request size and complexity stay at a reasonable level to ensure a [smooth review process](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria).
* If the merge request grows to an unexpected size or tries to solve too many issues at once, consider suggesting to the community contributor to split it into smaller ones and use a [feature flag](https://docs.gitlab.com/ee/development/feature_flags/) if necessary.
* If a merge request gets stalled for a long time, consider suggesting that you will finish the merge request. Check the proper section in [merge request coach responsabilities](https://about.gitlab.com/job-families/expert/merge-request-coach/) and the [closing policy for merge requests](https://docs.gitlab.com/ee/development/contributing/#closing-policy-for-issues-and-merge-requests).
  * Do not forget to credit the community contributor.
* If some aspects of the merge request become a high priority task (for example, a bug fix) and the work from the community contributor is less active or stalled, consider suggesting that you will extract this part to a small merge request to get it implemented as quickly as possible.
  * The other aspects with less priority can still be worked out by the community contributor.

### Sync Design Review

Design Feedback Round Robin is an effective tool to help enable teams by creating a structured feedback conversation. The primary focus of this exercise is to ensure everyone in the room has a voice, enabling us to capture a large quantity of precise and focused feedback. This conversation is strictly timeboxed to 15 minutes, so remember to be concise and have fun with it! If you ever need inspiration for feedback, consider taking a [few different hats for a spin](https://www.mindtools.com/pages/article/newTED_07.htm)!

##### The Set-Up
The setup is very simple. Before the sync session, prepare the agenda by pasting a link to these rules. As the meeting starts, look at the attending members' names and form a randomly ordered list. This will be the order for participants to go in. Make sure to put this ordering into the agenda.

##### The Process
The designer will kick off the process by quickly reviewing the rules and starting the 15-minute timer. After the timer has started, the activity goes as follows:

1.  The designer should present the design clearly communicating what areas they're looking for feedback on. For more genuine reactions and feedback, keep the explanation as short as possible.
1.  Following the order pasted into the agenda, participants take turns asking relevant questions and providing a single piece of feedback to the design. Each "turn" should be limited to about 1 minute.
1.  Repeat this turn-based process until time runs out or all the participants "pass".

#### The Turns
As a participant, you can do a few different things on your turn. Try to be quick, as each turn should only last around 1 minute. During your turn, you can do a few things (in order of priority):
1.  Ask questions to the designer.
1.  +1 or -1 somebody else piece of feedback.
1.  Provide **one(1)** piece of feedback.
1.  "Pass" - you can skip your turn.
1.  You officially end your turn by calling out the name of who is next.

Notes about the feedback options:
*  Feedback should revolve around the areas the designer has pointed out.
*  Feedback can be either positive, negative, or neutral. Helping a designer know what **is** working is as important as what could be improved.
*  One piece of feedback can build off of another person's feedback.

Remember, the goal is to capture a quantity of specific feedback. While it may be tempting to start discussions around the design choices, this activity doesn't make for a good forum. The designer will follow up with discussions asynchronously afterward in the issue to start discussions and conversations around the feedback.


### Async Daily Standups

The purpose of the daily standup is to allow team members to have visibility into what everyone else is doing, allow a platform for asking for and offering help, and provide a starting point for some social conversations. We use [geekbot](https://geekbot.com/) integrated with Slack.

While it is encouraged to participate in the full variety of daily questions, it is completely acceptable to skip questions by entering `-`.

A quick status of the various issues and merge requests that are currently being worked on is suggested. If the work is in progress, provide the estimated percentage complete, and the percentage confidence level in that estimate.

> `!12345 - 50% complete, 85% confident` - Working on tests and refactoring

This would mean that you are roughly halfway to being code complete with merge request 12345, and are fairly confident that your estimate is correct. A low confidence percentage is a way of identifying that you still believe there is much to uncover in what work is left.

It may be helpful to provide some commentary on what piece/aspect of the MR or issue you are working on as well. Once the MR is code complete and in review, you can leave off the percentages and simply say `in review` or `in maintainer review`.

### Weekly Retrospective

Our weekly retrospective is intended to provide the team an opportunity to retrospect on our week's effort. The discussion takes the usual GitLab asynchronous endabled synchronous meeting format: it has an agenda google document and we upload the video to [GitLab Unfiltered](https://youtube.com/c/GitLabUnfiltered). We currently have our retro every week on Friday morning (UTC-7) and, every 3rd week the meeting is held on Thursday afternoon (UTC-7) to support people in APAC TZs. The retrospective is a 25 minute long meeting.

The document is an ongoing list of retrospectives with a date heading as well as a link to the video after it has been uploaded. Each retrospective is divided into `what went not so well` and `what went well` in that order - so we can end the meeting on a positive note. In asynchronous style, we add our items prior to the meeting and read them out during the meeting. We read people's items when they aren't able to attend.

We roll up some of our retro thoughts into our monthly, milestone-linked, async retrospective. Ideally we will be able to address concerns in the retro. Action items are described during the meeting.

Examples of our retrospectives can be found here: [https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=package+retro](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=package+retro)

### Issue Weighting

| Weight | Description  |
| --- | --- |
| 1: Trivial | The problem is very well understood, no extra investigation is required, the exact solution is already known and just needs to be implemented, no surprises are expected, and no coordination with other teams or people is required.<br><br>Examples are documentation updates, simple regressions, and other bugs that have already been investigated and discussed and can be fixed with a few lines of code, or technical debt that we know exactly how to address, but just haven't found time for yet.<br><br>This will map to a confidence greater or equal to 90%. |
| 2: Small | The problem is well understood and a solution is outlined, but a little bit of extra investigation will probably still be required to realize the solution. Few surprises are expected, if any, and no coordination with other teams or people is required.<br><br>Examples are simple features, like a new API endpoint to expose existing data or functionality, or regular bugs or performance issues where some investigation has already taken place.<br><br>This will map to a confidence greater than or equal to 75%. |
| 3: Medium | Features that are well understood and relatively straightforward. A solution will be outlined, and most edge cases will be considered, but some extra investigation will be required to realize the solution. Some surprises are expected, and coordination with other teams or people may be required.<br><br>Bugs that are relatively poorly understood and may not yet have a suggested solution. Significant investigation will definitely be required, but the expectation is that once the problem is found, a solution should be relatively straightforward.<br><br>Examples are regular features, potentially with a backend and frontend component, or most bugs or performance issues.<br><br>This will map to a confidence greater than or equal to 60%. |
| 5: Large | Features that are well understood, but known to be hard. A solution will be outlined, and major edge cases will be considered, but extra investigation will definitely be required to realize the solution. Many surprises are expected, and coordination with other teams or people is likely required.<br><br>Bugs that are very poorly understood, and will not have a suggested solution. Significant investigation will be required, and once the problem is found, a solution may not be straightforward.<br><br>Examples are large features with a backend and frontend component, or bugs or performance issues that have seen some initial investigation but have not yet been reproduced or otherwise "figured out".<br><br>This will map to a confidence greater than or equal to 50%. |

Anything larger than 5 should be broken down. Anything with a confidence percentage lower than 50% should be investigated prior to finalising the issue weight.

## Common Links

* [Package backend board]
* [#g_package] in Slack
* [Recorded meetings][youtube]
* [Retrospectives][retros]
* [Timezones][timezones]

[Package backend board]: https://gitlab.com/groups/gitlab-org/-/boards/892745
[#g_package]: https://gitlab.slack.com/archives/g_package
[youtube]: https://www.youtube.com/playlist?list=PL05JrBw4t0KoPiSySNHTfvxC20i0LppMf
[retros]: https://gitlab.com/gl-retrospectives/package/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective
[timezones]: https://timezone.io/team/gitlab-package-team
