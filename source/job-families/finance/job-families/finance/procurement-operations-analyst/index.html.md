---
layout: job_family_page
Title: "Procurement Operations Analyst"
---

The Procurement Operations Analyst supports procurement and spend management activities for all spend categories. They will work cross-functionally to analyze spend data, research suppliers, and support the preparation and analysis of strategic sourcing events. 

## Responsibilities
* Support the Procurement Team and internal stakeholders for procurement and sourcing related initiatives.
* Maintain and update the procurement process to meet the business needs in a rapid manner.
* Support the business stakeholders with requisition and process questions or challenges.
* Conduct analysis in various tools to support procurement activities including spend analysis, budget analysis, bid analysis, etc.
* Leverage spend analytics to identify consolidation opportunities, understand total cost of ownership, and rationalize opportunities.
* Update and leverage data within multiple enterprise software solutions to support business objectives.
* Analyze market intelligence to build business cases and cost models to identify and quantify improvement opportunities for demand management, process improvements, build vs. buy, etc.
* Develop and maintain procurement metrics (Performance Indicators, benchmarking & SLAs) with collaboration of procurement and internal stakeholder teams.
* Provide procurement reporting on a recurring basis.

## Requirements
* Bachelor's degree in relevant field
* Ability to navigate cultural differences and build global but locally relevant solutions
* Strong social and communication skills (verbal and written), across all levels
* Excellent organizational skills, time management, and priority setting
* Deadline oriented, and able to work in a fast-paced environment with ever-changing priorities
* Self-motivated with the ability to work both independently and collaboratively
* Proficient in Google Docs
* You share our values, and work in accordance with those values
* Successful completion of a background check 
* Ability to use GitLab

## Performance Indicators
* [Percent of Vendor Spend on Purchase Order](/handbook/business-ops/metrics/#percent--of-vendor-spend-on-purchase-order)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
Additional details about our process can be found on our [hiring page](/handbook/hiring).
* Qualified candidates will be invited to schedule a 30 minute screening call with a member of our recruiting team
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Procurement Manager
* After that, candidates will be invited to schedule a 30 minute interview with a Business Systems Analyst
* Next, the candidate will be invited to interview with a Finance Business Partner
* Next, candidates will be invited to schedule a 30 minute interview with our Director of Business Operations
* Finally, our CEO may choose to conduct a final interview
