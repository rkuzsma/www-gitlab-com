---
layout: markdown_page
title: "FY21-Q1 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2020 to April 30, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO Objective: IACV
1. CEO KR: End Q1 with Q2 New business IACV Stage 3+ [Pipeline](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) and Growth business IACV Stage 3+ Pipeline both ahead of plan.
1. CEO KR: Maintain quota capacity at least 5% ahead of plan for FY21.  Exceed Q1 [NQR](/handbook/sales/commissions/#quotas-overview) [hiring](/handbook/hiring/charts/sales-nqr/)) by at least 5% quota coverage.
   1. **CRO: Hire NQRs to Q1 plan**
      1. KR: NQR hire start dates in Q1 exceed plan
      1. KR: Recruiting program at least 5% above capacity to hit plan
      1. KR: Overlay hires with start dates in Q1 at or above plan 
1. CEO KR: Renewal process satisfaction, as measured by [SSAT](/handbook/business-ops/data-team/kpi-index/#satisfaction) for tickets about renewals, is greater than 95%.
   1. **Director of Product, Growth: Improve the customer experience with billing related workflows.** [gitlab-com&263](https://gitlab.com/groups/gitlab-com/-/epics/263)
      1. Director of Product, Growth KR: Make material improvements to direct signup, trial, seat true-up, upgrade, and renewal workflows, both for .com and self-hosted customers. [gitlab-com/Product#725](https://gitlab.com/gitlab-com/Product/issues/725)
      1. Director of Product, Growth KR: Drive [Support Satisfaction](/handbook/support/performance-indicators/#support-satisfaction-ssat) scores, filtered for billing, to >95%. [gitlab-com/Product#726](https://gitlab.com/gitlab-com/Product/issues/726)
      1. Director of Product, Growth KR: Drive [$561k in incremental IACV](https://gitlab.com/gitlab-org/growth/product/issues/805). [gitlab-com/Product#727](https://gitlab.com/gitlab-com/Product/issues/727)
1. **VP of Product Management: Complete pricing analysis and, if required, roll out related updates to GitLab's pricing and packaging.** [gitlab-com&264](https://gitlab.com/groups/gitlab-com/-/epics/264)
   1. VP of Product Management KR: Complete pricing analysis project and, if required, implement at least the first phase of any recommended changes. [gitlab-com/Product#728](https://gitlab.com/gitlab-com/Product/issues/728)
1. **CRO: Launch New Channel Program** 
   1. CRO KR: New tiers, terms and agreement complete, published on partner portal and handbook for all partners
   1. CRO KR: Automated deal reg launched and available in portal 
   1. CRO KR: Deliver at least one new PIO in every region through portal
1. **CRO: Increase stage usage by at least one stage for 10 of top 50 accounts with only 1-2 stages in use as measured by SMAU >10% per stage.** 
   1. CRO KR: Targeted account based plan to offer services and support to utilize a new stage at each of the 50 targeted accounts.
   1. CRO KR: 10 accounts add at least one stage with SMAU moving from <5% to >25% in a specific stage.
1. **EVP of Engineering**
   1. **Sr Director of Development: [Support incremental IACV](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6237)**
      1. Sr Director of Development KR: Deliver the top 10 customer requested features as prioritized by Product Management
      1. **Director of Engineering, Dev: [Support incremental IACV](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6243)**
         1. Director of Engineering, Dev KR: Deliver the top 2 customer-requested features as prioritized by Product Management in each stage
         1. Director of Engineering, Dev KR: Deliver 15 items in the performance or Infra/Dev dashboard
      1. **Director of Engineering, Enablement: [Continue efforts to build enterprise-grade SaaS](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6205)**
         1. Director of Engineering, Enablement KR: Complete dogfooding Geo on GitLab.com staging environment [gitlab-org&575](https://gitlab.com/groups/gitlab-org/-/epics/575)
         1. Director of Engineering, Enablement KR: Enable ElasticSearch for paid groups on GitLab.com [gitlab-org&1736](https://gitlab.com/groups/gitlab-org/-/epics/1736)
         1. Director of Engineering, Enablement KR: Scope and start MVC of database partitioning, related epic [gitlab-org&2023](https://gitlab.com/groups/gitlab-org/-/epics/2023), completion of MVC may extend into Q2 due to dependency of PostgreSQL 11 upgrade.
      1. **Director of Engineering, Defend: [3 new Defend features go from planned to minimal](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6192)**
         1. Director of Engineering, Defend KR: 3 new Defend MVC features go from planned to minimal, per PM priorities
         1. Director of Engineering, Defend KR: Track all planning priority issues and make sure they are being completed on time
      1. **Director of Engineering, Growth: [Deliver Improved product usage data](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6240)**
         1. Director of Engineering, Growth KR: Implement Telemetry Strategy
         1. Director of Engineering, Growth KR: Support repeatable experiments across gitlab.com and the customers application
         1. Director of Engineering, Growth KR: Track all planning priority issues and make sure they are being completed on time
         
### 2. CEO: Popular next generation product
1. CEO KR: [SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau) is being measured and reported in 100% of Sales and Product [Key Meetings](/handbook/finance/key-meetings/).
   1. **Director of Product, Growth:  Ensure accurate data collection and reporting of AMAU and SMAU metrics.** [gitlab-com&265](https://gitlab.com/groups/gitlab-com/-/epics/265)
      1. Director of Product, Growth KR: Deliver AMAU and SMAU tracking with less than 2% uncertainty. [gitlab-com/Product#729](https://gitlab.com/gitlab-com/Product/issues/729)
1. CEO KR: [SPU](/handbook/product/metrics/#stages-per-user) increases by 0.25 stages from EOQ4 to EOQ1.
1. CEO KR: [MAU](/handbook/product/metrics/#monthly-active-users-mau) increases 5% from EOQ4 to EOQ1.
1. **VP of Product Management: Proactively validate problems and solutions with customers.** [gitlab-com&266](https://gitlab.com/groups/gitlab-com/-/epics/266)
   1. VP of Product Management KR: At least 2 validation cycles completed per Product Manager. [gitlab-com/Product#730](https://gitlab.com/gitlab-com/Product/issues/730)
1. **VP of Product Management: Create demos of the top competitor in each stage to compare against our own demos completed in Q4.** [gitlab-com&267](https://gitlab.com/groups/gitlab-com/-/epics/267)
   1. VP of Product Management KR: Deliver one recorded demo for each stage. [gitlab-com/Product#731](https://gitlab.com/gitlab-com/Product/issues/731)
1. **Principal Product Manager, Product Operations: Roll out [Net Promoter Score (NPS)](/handbook/product/metrics/#paid-net-promoter-score) tracking.** [gitlab-com&268](https://gitlab.com/groups/gitlab-com/-/epics/268)
   1. Principal Product Manager, Product Operations KR: Survey at least 25% of GitLab's paid customer base, with a reponse rate of >4%. [gitlab-com/Product#732](https://gitlab.com/gitlab-com/Product/issues/732)
1. **Director, Product, Ops:  Work with GitLab's infrastructure team to dogfood our APM metrics.** [gitlab-com&269](https://gitlab.com/groups/gitlab-com/-/epics/269)
   1. Director, Product, Ops KR:  Ensure APM metric dogfooding is properly prioritized. [gitlab-com/Product#733](https://gitlab.com/gitlab-com/Product/issues/733)
1. **VP of Product Strategy: Get strategic thinking into the org.** 
    1. VP of Product Strategy KR: Secure and Enablement section strategy reviews
    1. VP of Product Strategy KR: Produce [strategy visuals](https://gitlab.com/gitlab-com/Product/issues/512)
1. **VP of Product Strategy: Lay groundwork for strategic initiatives.** 
    1. VP of Product Strategy KR: Hire strategic initiatives team
1. **Sr. Director of Corp Dev: Get acquisitions into shape; build a well-oiled machine.** 
    1. Sr. Director of Corp Dev KR: Identify 1000 [qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets).
1. **EVP of Engineering:**
    1. Director of UX: [Increase the value of category maturity ratings by validating them with users](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
        1. Director of UX KR: Validate category maturity ratings with users for all categories that moved to the next rating (except Minimal, which is self assigned) during Q4 FY20.
    1. Director of UX: [Empower Product Managers and Designers to independently conduct UX Scorecard validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6203)
         1. Director of UX KR: Update UX handbook with helpful context and tips, linking to existing research training where applicable.
    1. **Sr Director of Development: [Deliver on our product vision by being more iterative](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6230)**
         1. Sr Director of Development KR: Reduce Average Review to Merge Time by 1 day
         1. Sr Director of Development KR: Increase MR Rate from topline goal of 10 to 11
         1. Sr Director of Development KR: Based on Started to Shipped % measure improve team performance
         1. **Director of Engineering, Dev: [Improve section productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6241)**
            1. Director of Engineering, Dev KR: Overall section MR/engineer Rate increases to >=11 by the end of April.
            1. Director of Engineering, Dev KR: Sections Mean time to merge below 12 days
            1. Director of Engineering, Dev KR: Reduce Onboarding Time by Creating a Development Bootcamp video series
         1. **Director of Engineering, Defend: [Accelerate defend productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6193)**
            1. Director of Engineering, Defend KR: >=1 defend engineer volunteers becomes a trainee maintainer and starts the process to become one by the end of the quarter
            1. Director of Engineering, Defend KR: Maintain current developers to maintainers ratio - Nominate and add maintainers as we are increasing (BackEnd increase from X to Y, Frontend increase by X to Y)
            1. Director of Engineering, Defend KR:  Fully document and where possible fully automate both creation and validation of defend back-end development (autodevops with local GDK) environment by the end of the quarter
         1. **Director of Engineering, Secure: [Increase Productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6198)**
            1. Director of Engineering, Secure KR: Increase rolling 6 month average by >=10%
         1. **Director of Engineering, CI/CD: [Improve monthly MR Rate](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6234)**
            1. Director of Engineering, CI/CD KR: Overall section MR Rate increases to 10 by the end of April
            1. Director of Engineering, CI/CD KR: Improve Say/Do to 70%, measured by Deliverable items delivered in each milestone
            1. Director of Engineering, Secure KR: Increase rolling 6 month average by >=10%
         1. **Director of Engineering, Ops: [Dogfood Monitoring](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6207)**
            1. Director of Engineering, Ops KR: Identify with stakeholders (e.g. PM) key dogfooding metrics
            1. Director of Engineering, Ops KR: Define engineering plan to drive dogfooding of APM features
            1. Director of Engineering, Ops KR: Target an improvement in key dogfooding metric(s)
         1. **Director of Engineering, Ops: [Increase Section MR Rate](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6208)**
            1. Director of Engineering, Ops KR: Section MR Rate increases 20% month over month through April.
         1. **Director of Engineering, Enablement: [Improve monthly MR Rate](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6204)**
            1. Director of Engineering, Enablement KR: Overall section MR Rate increases to 10~12 by the end of April.
            1. Director of Engineering, Enablement KR: Track all Deliverable issues and achieve 60~70% delivery rate in each release.
            1. Director of Engineering, Enablement KR: Keep BE maintainer ratio 8~9:1.
         1. **Director of Engineering, Growth: [Improve section productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6239)**
            1. Director of Engineering, Growth KR: Overall section MR Rate increases from approx 9 to 11 by the end of April.
            1. Director of Engineering, Growth KR: Achieve FE and BE maintainer ratio 7:1.
            1. Director of Engineering, Growth KR: At least one deliverable per week, per growth group (48 in total).

### 3. CEO: Great team
1. CEO KR: Teams are working [handbook-first](/handbook/handbook-usage/#why-handbook-first) and no content is being produced in other tools, e.g. Docs, Classroom, etc.
1. CEO KR: There 7 certifications online for all community members to pursue.
1. CEO KR: There are 200 certifications completed by [middle managers](/company/team/structure/#middle-management) at GitLab.
1. **EVP of Engineering:**
    1. Director of UX: [Provide clear career paths for everyone in UX](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6081)
        1. Director of UX KR: Create and update Technical Writing roles
        1. Director of UX KR: Create and update UX Research roles
        1. Director of UX KR: Create and update Product Design roles
    1. **Sr Director of Development: [Build a world class software team](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6231)**
        1. Sr Director of Development KR: Meet Q1 Hiring plan for development
        1. Sr Director of Development KR: Implement at least one Culture Amp action item
        1. Sr Director of Development KR: Have Senior Manager plans in place and at least X positions open
        1. **Director of Engineering, Dev: [Hire and Scale section to target](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6242)**
           1. Director of Engineering, Dev KR: Hire according to plan
           1. Director of Engineering, Dev KR: Introduce and hire at least 1 senior manager in Q1
        1. **Director of Engineering, Secure: [Hiring to Target](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6197)**
        1. **Director of Engineering, Defend: [Hiring to Target](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6191)**
           1. Director of Engineering, Defend KR: Fill remaining open positions for defend team (7 new hires)
        1. **Director of Engineering, CI/CD: [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6235)**
        1. **Director of Engineering, Ops: [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6206)**
        1. **Director of Engineering, Enablement: [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6202)**
           1. Director of Engineering, Enablement KR: Hire according to plan.
           1. Director of Engineering, Enablement KR: Improve engineer interviewer ratio from 1:3.7 to 1:3.
           1. Director of Engineering, Enablement KR: Introduce and hire at least 1 senior manager in Q1.
        1. **Director of Engineering, Growth: [Accomplish Q1 hiring goals](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6238)**

## How to Achieve Presentations
