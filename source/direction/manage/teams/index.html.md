---
layout: markdown_page
title: "Category Direction - Teams"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [Category:Teams](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ATeams) |

## Teams

Groups in GitLab are intentionally flexible. They're a great way of creating organization and access control, and do a good job of being a relatively generic object that's effective at both managing projects and organizing groups of people. In fact, our hypothesis is that these are the two big buckets that users tend to lean on groups for:

* Managing what gets worked on (the code). An organization might make a new group for a dedicated initiative and all its child projects. This specific namespace helps manage code that spans multiple projects.

* Managing the teams that do the work (the people). A group might be an organizational tool for a particular functional area (Sales, Marketing), and largely lean on issues to get things done.

While we do a solid job on the former, we've got room to improve on the latter. Since groups do double-duty here and are relatively generic objects, we're losing an opportunity to refine specific features that might help people get work done. Specifics on where we see this evidenced:

* Reporting
* Access control
* Communication
* Portfolio management and workflows

## Target audience and experience

Whilst we can assume that all users would benefit from the Team experience, the target personas we are focusing on are Team Leaders, [Group Owners](https://docs.gitlab.com/ee/user/permissions.html#permissions) & System Administrators.

### Current focus

Please see the [Create and manage a team in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/122) epic.

<!-- ## What's next & why -->

## Maturity

For the moment, Teams are considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions. 

## How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/603).
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=user%profile&label_name[]=Accepting%20merge%20requests)!

<!-- ## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD

-->
